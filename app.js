var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sassMiddleware = require('node-sass-middleware');
var session = require('express-session');
var favicon = require('serve-favicon');
var methodOverride = require('method-override');
var passport = require('passport');
var flash = require('connect-flash');
var mongoose = require('mongoose');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var toursRouter = require('./routes/tours');

var passportConfig = require('./lib/passport-config');

var app = express();
 
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

// mongodb 연결
mongoose.Promise = global.Promise;
mongoose.connect(
  'mongodb+srv://kgh9959:kgh951220@triptipdream-0ziri.mongodb.net/test?retryWrites=true&w=majority',
  {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
  }
);
const db = mongoose.connection;
db.once('open', () => console.log("몽고DB 연결완료"));
db.on('error', error => console.log("몽고DB 에러발생: ${error}"));

// passport 초기화
app.use(passport.initialize());
app.use(passport.session());
passportConfig(passport); // 여기서 passport 내부를 셋팅..

// public 디렉토리에 있는 내용을 static하게 서비스
app.use(express.static(path.join(__dirname, 'public')));

// session 사용
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: '!@#!@#-squat-!@#$!@#-deadlift-!@#$!#@$-benchpress-secret!@#'
}));

// 메소드 오버라이딩
app.use(methodOverride('_method', {methods: ['POST', 'Get']}));

// 플래시 메세지 사용
app.use(flash());

// pug의 local에 현재 사용자 정보와 flash 메세지를 전달하자?
app.use(function(req, res, next){
  res.locals.currentUser = req.user;
  res.locals.flashMessages = req.flash();
  next();
});

// pug의 loacl에서 moment라이브러리(날짜관련 작업)과 querystring라이브러리(URL에 "?blabla=" 이런거 쓰는거)를 사용
app.locals.moment = require('moment');
app.locals.querystring = require('querystring');

// favicon 사용
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// 라우터 연결
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/tours', toursRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
}); 

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
}); 

module.exports = app;
