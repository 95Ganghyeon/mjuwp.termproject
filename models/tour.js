const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const Schema = mongoose.Schema;

var schema = new Schema({
  location: {type: String},
  title: {type: String, required: true},
  price: {type: Number, required: true},
  explanation: {type: String},
  course: [String],

  guide: {type: Schema.Types.ObjectId, ref: 'User'},
  createdAt: {type: Date, default: Date.now}
},{
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.plugin(mongoosePaginate);
var Tour = mongoose.model('Tour', schema);
module.exports = Tour;
