const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;

mongoose.set('useCreateIndex', true);

// 스키마 생성
var schema = new Schema({
  name: {type: String, required: true, trom: true},
  email: {type: String, required: true, index: true, unique: true, trim: true},
  password: {type: String},
  facebook: {id: String, token: String},
  naver: {id: String, token: String},
  createdAt: {type: Date, default: Date.now},
  level: {type: String, required: true}
},{
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

// 비밀번호 함후와 기능을 위한 함수를 객체 모듈에 담아서 export 할것임
schema.methods.generateHash = function(password){
  return bcrypt.hash(password, salt=2019);
};

// 비밀번호 찾기기능 제공을 위한 함수를 객체 모듈에 담아서 export 할것임
schema.methods.validatePassword = function(password){
  return bcrypt.compare(password, this.password);
};

var User = mongoose.model('User', schema);
module.exports = User;
