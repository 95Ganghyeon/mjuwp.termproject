const LocalStrategy = require('passport-local').Strategy; // 구글에 passport-local을 쳐서 official document를 확인해봐라
const FacebookStrategy = require('passport-facebook').Strategy;
const User = require('../models/user');

module.exports = function(passport) { // 이 기다란 오브젝트 module.exports라는 함수 하나가 넘어가는 방식이다. 
                                      // app.js에 가면 이 함수를 var passportConfig로 받고 있다.
  passport.serializeUser((user, done) => { // 패스포트는 반드시 시리얼라이즈하고 디시리얼라이즈 하는 과정이 반드시 필요하다고 했다.
    done(null, user.id); // 시리얼라이즈는 어떠한 유저 하나를 유니크한 스트링값으로 만들어내는 과정이었다.  
  }); // 내부에서 에로우 펑션으로 어떻게 할지를 구현해서 serializeUser()라는 함수에 담아줬다.

  passport.deserializeUser((id, done) =>  {
    User.findById(id, done); // 여기서 err가 발생하면 done에 err를 담아서 넘어갈 것이고, 제대로 되면 제대로 넘어갈 것이다. 굳이 done을 따로 if-else 펑션으로 따로 만들 필요가 없다. 
  });

  passport.use('local-signin', new LocalStrategy({ // 이 유저가 valid한 유저인지 아닌지를 판단하는 부분
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true
  }, async (req, email, password, done) => { // passport-local을 보면 왜 done이 들어가 있는지 알 수 있을 것이다...
    try {
      const user = await User.findOne({email: email});
      if (user && await user.validatePassword(password)) { // 여기서 등장하는 validatePassword()라는 함수는 user.js라는 model에 구현되어 있던 함수였다. 
        return done(null, user, req.flash('success', 'Welcome!'));
      }
      return done(null, false, req.flash('danger', 'Invalid email or password'));
    } catch(err) {
      done(err); // 에러가 나면 그냥 콜백에다가 에러를 담아서 보내주는 것이다.(왠만한 콜백은 항상 첫번째 인자로 err를 설정한다?)
    }
  })); // 결국 여기서 내가 한일은 유저가 입력한 패스워드가 같은지 아닌지를 판단해서 flash를 띄워준것뿐이다.

  passport.use(new FacebookStrategy({
    // 이 부분을 여러분 Facebook App의 정보로 수정해야 합니다!!!!!!!!!
    // 이 앱은 교수님 계정으로 '개발중'으로 페이스북에 요청된 상태이기 때문에, 우리 아이디로 백날 페북로그인해봐야 안될 것이다.
    // 내가 facebook for developer 웹페이지로 들어가서 clientID와 clientSecret을 받아와야 한다.
    clientID : '185081248729582',
    clientSecret : 'f02d8be90007cf67bd16495033e1b95a',
    callbackURL : 'http://127.0.0.1:3000/auth/facebook/callback', // URL은 거의 무조건 이렇게 나올 것이다.
    profileFields : ['email', 'name', 'picture']
  }, async (token, refreshToken, profile, done) => {
    console.log('Facebook', profile); // profile 정보로 뭐가 넘어오나 보자.
    try {
      var email = (profile.emails && profile.emails[0]) ? profile.emails[0].value : ''; // 페이스북은 이메일을 여러개 등록해 놓을 수 있어서 배열로 이메일을 넘겨준다.
                                                                                        // 그래서 우리는 그 배열에서 첫번째 이메일을 쓰겠다는 것이다.
      var picture = (profile.photos && profile.photos[0]) ? profile.photos[0].value : '';
      var name = (profile.displayName) ? profile.displayName : 
        [profile.name.givenName, profile.name.middleName, profile.name.familyName] // 페북이 미국서비스이기 때문에 name이라는 것을 이렇게 해괴하게 구분해서 관리하고 있다.
          .filter(e => e).join(' '); // 그래서 우리는 그것들을 하나로 합쳐친 스트링을 만들어서 그것을 name으로 다루기로 한 것이다. 
      console.log(email, picture, name, profile.name);
      // 같은 facebook id를 가진 사용자가 있나?
      var user = await User.findOne({'facebook.id': profile.id});
      if (!user) {
        // 없다면, 혹시 같은 email이라도 가진 사용자가 있나?
        if (email) {
          user = await User.findOne({email: email});
        }
        if (!user) {
          // 그것도 없다면 새로 만들어야지.. 왜냐고? 애초에 user.js에서 모델을 정의할 때 email을 required: true로 놓았고, email을 unique: true로 놓았기 때문이다.
          user = new User({name: name});
          user.email =  email ? email : `__unknown-${user._id}@no-email.com`;
        }
        // facebook id가 없는 사용자는 해당 id를 등록
        user.facebook.id = profile.id;
        user.facebook.photo = picture;
      }
      user.facebook.token = profile.token;
      await user.save();
      return done(null, user);
    } catch (err) {
      done(err);
    }
  }));
};
