const express = require('express');
const Tour = require('../models/tour');
const catchError = require('../lib/async-error');

const router = express.Router();

// GET 홈페이지
router.get('/', (req, res, next) => {
  res.render('index');
}); 

// GET 검색창
router.get('/search', catchError(async (req, res, next) => {
  const page = parseInt(req.query.gage) || 1;
  const limit = parseInt(req.query.limit)  || 10;

  var query = {};
  const term = req.query.term;
  if (term){
    query = {$or: [
      {location: {'$regex': term, '$options': 'i'}},
      {title: {'$regex': term, '$options': 'i'}},
      {explanation: {'$regex': term, '$options': 'i'}}
    ]};
  }
  const tours = await Tour.paginate(query, {
    sort: {createdAt: -1},
    populate: 'guide',
    page: page, limit: limit
  });
  res.render('tours/index', {tours: tours, term: term, query: req.query});
}));



module.exports = router;
