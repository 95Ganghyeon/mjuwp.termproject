const express = require('express');
const Tour = require('../models/tour');
const catchError = require('../lib/async-error');

const router = express.Router();

/* GET users listing. */
router.get('/index', function(req, res, next) {
  res.render('tours/index');
});



module.exports = router;
