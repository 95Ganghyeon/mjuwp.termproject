const express = require('express');
const User = require('../models/user');
const router = express.Router();
const catchErrors = require('../lib/async-error');

function needAuth(req, res, next){
  if(req.isAutehnticated()){
    next();
  } else{
    req.flash('danger', '로그인이 필요합니다.');
    res.redirect('/signin');
  }
}

function validateForm(form, options){
  var name = form.name || "";
  var email = form.email || "";
  name = name.trim();
  email = email.trim();

  if(!name){
    return '멋진 이름이 있으시잖아요.';
  }
  if(!email){
    return '이메일 주소를 입력해주세요.';
  }
  if(!form.password && options.needPassword){
    return '비밀번호를 입력해주세요.';
  }
  if(form.password !== form.password_confirmation){
    return '비밀번호가 일치하지 않습니다.';
  }
  if(form.password.length < 8){
    return '비밀번호는 8글자 이상이어야 합니다.';
  }
}

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});




// 로그인 페이지
router.get('/signin', (req, res, next) => {
  res.render('users/signin');
});

// 회원가입 페이지
router.get('/new', (req, res, next) => {
  res.render('users/new');
});

// 회원가입 유저정보를 서버로 POST 
router.post('/', catchErrors(async (req, res, next) => {
  var err = validateForm(req.body, {needPassword: true});
  if(err){
    req.flash('danger', err);
    return res.redirect('back');
  }

  var user = await User.findOne({email: req.body.email});
  if(user){
    req.flash('danger', '이미 가입된 이메일이 존재합니다.');
    return res.redirect('back');
  }

  user = new User({
    name: req.body.name,
    email: req.body.email
  });
  user.password = await user.generateHash(req.body.password);
  await user.save();
  req.flash('success', '회원가입이 완료되었습니다. 다시 로그인 해주세요.');
  res.redirect('/');
}));

module.exports = router;
